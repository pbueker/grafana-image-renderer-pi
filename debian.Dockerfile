FROM node:lts-buster AS base

ENV CHROME_BIN="chromium"
ENV PUPPETEER_SKIP_CHROMIUM_DOWNLOAD="true"

WORKDIR /usr/src/app

RUN apt-get update \
	&& apt-get install -y gnupg wget dumb-init \
      --no-install-recommends \
#	&& wget --no-check-certificate -q -O - https://dl-ssl.google.com/linux/linux_signing_key.pub | apt-key add - \
#    && sh -c 'echo "deb [arch=armhf] http://dl.google.com/linux/chrome/deb/ stable main" >> /etc/apt/sources.list.d/google.list' \
#    && apt-get update \
    && apt-get install -y chromium fonts-ipafont-gothic fonts-wqy-zenhei fonts-thai-tlwg fonts-kacst fonts-freefont-ttf \
      --no-install-recommends \
    && rm -rf /var/lib/apt/lists/*
#    && wget --no-check-certificate -q http://ftp.de.debian.org/debian/pool/main/d/dumb-init/dumb-init_1.2.0-1_armhf.deb \
#    && tar -x *.deb \
#    && tar -xvf data.tar.xz
#	&& dpkg -i *.deb

#ADD https://github.com/Yelp/dumb-init/releases/download/v1.2.0/dumb-init_1.2.0_amd64 /usr/local/bin/dumb-init
#RUN chmod +x /usr/local/bin/dumb-init

FROM base as build

#COPY . ./

RUN wget --no-check-certificate -q https://gitlab.com/pbueker/grafana-image-renderer-pi/-/archive/master/grafana-image-renderer-pi-master.tar.gz \
	&& tar -xvf grafana-image-renderer-pi-master.tar.gz -C . --strip-components=1

RUN yarn install --pure-lockfile
RUN yarn run build

EXPOSE 8081

CMD [ "yarn", "run", "dev" ]

FROM base

ENV NODE_ENV=production

COPY --from=build /usr/src/app/node_modules node_modules
COPY --from=build /usr/src/app/build build
COPY --from=build /usr/src/app/proto proto
COPY --from=build /usr/src/app/default.json config.json

EXPOSE 8081

ENTRYPOINT ["dumb-init", "--"]

CMD ["node", "build/app.js", "server", "--config=config.json"]